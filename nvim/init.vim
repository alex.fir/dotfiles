source $HOME/.config/nvim/vim-plug/plugins.vim
" source $HOME/.config/nvim/themes/onedark.vim

set noshowmode
set number

set tabstop=4 softtabstop=4
set smartindent
set shiftwidth=4
set expandtab
set termguicolors

set mouse=a

highlight Normal guibg=None

" colorscheme aura
colorscheme horizon

let g:lightline = {
    \ 'colorscheme': 'horizon',
    \ }

" mapping
let mapleader = " "

" I mean, who doesn't like comments
nnoremap <C-/> :AutoInLineComment
vnoremap <C-/> :AutoInLineComment

" Quick launch some files
nnoremap <C-e>i :e /home/tux/.config/nvim/init.vim <CR>
nnoremap <C-e>v :e /home/tux/.config/nvim/vim-plug/plugins.vim <CR>

" NERDTree
nnoremap <C-n> :NERDTreeToggle <CR>

" Tabs
" <A..> - the alt key

" Move between tabs
nnoremap <silent> <A-,> <Cmd>BufferPrevious<CR>
nnoremap <silent> <A-.> <Cmd>BufferNext<CR>

" Close the tab
nnoremap <silent> <A-c> <Cmd>BufferClose<CR>
